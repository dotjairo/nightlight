#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

//! button states
enum buttonStates
{
    //! Depressed
    NotPressed,
    //! button is pressed but it is uncertain
    Pressed,
    //! button was released
    Released
};

typedef struct button
{
    //! True if button is currently pressed
    bool pressed;
    //! Timestamp when button was pressed
    unsigned int timePressed;
    //! Elapsed time that the button has been pressed
    unsigned int timer;
    //! State of button
    buttonStates state;
} button_t;

/**
 * @brief Update state of button
 * @params [in] btn State of button
 * @params [in] isPressed Current state of GPIO pin
 */
button_t button_update(button_t btn, bool isPressed)
{
    // if button is pressed
    if(isPressed)
    {
        // if not previously pressed
        if(!btn.pressed)
        {
            btn.timePressed = millis();
            btn.timer = 0;
            btn.state = Pressed;
        }
        // if was already pressed update timer
        else
        {
            btn.timer = millis() - btn.timePressed;
        }
    }
    // if not pressed, but previously pressed (just released)
    else if(!isPressed && btn.pressed)
    {
        btn.state = Released;
    }
    else
    {
        btn.state = NotPressed;
    }

    // save current state
    btn.pressed = isPressed;

    // return copy of state
    return btn;
}

#ifdef __cplusplus
}
#endif
